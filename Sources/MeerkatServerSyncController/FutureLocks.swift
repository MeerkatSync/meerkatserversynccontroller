//
//  FutureLocks.swift
//  
//
//  Created by Filip Klembara on 08/03/2020.
//

import Foundation

struct Prom {
    let id: Int
    let promise: Promise<Void>
}

class PromiseLock {
    enum State {
        case writeLocked
        case readLocked
        case done
    }

    var isFinished: Bool { state == .done }

    private var state: State = .done

    private var counter = 0

    private func nextCounter() -> Int {
        defer { counter = counter + 1}
        return counter
    }

    private var reads: [Prom] = []
    private var writes: [Prom] = []

    func lockRead(eventLoop: EventLoop) -> Prom {
        let promise = eventLoop.newPromise(Void.self)
        let prom: Prom = .init(id: nextCounter(), promise: promise)
        reads.append(prom)
        if writes.isEmpty {
            state = .readLocked
            promise.succeed()

        }
        return prom
    }

    func unlockRead(id: Int) {
        guard let i = reads.firstIndex(where: { $0.id == id }) else {
            assertionFailure()
            return
        }
        reads.remove(at: i)
        if let w = writes.first {
            let r = reads.first?.id ?? .max
            if w.id < r {
                state = .writeLocked
                w.promise.succeed()
            }
        } else if reads.isEmpty {
            state = .done
        }
    }

    func lockWrite(eventLoop: EventLoop) -> Prom {
        let promise = eventLoop.newPromise(Void.self)
        let prom = Prom(id: nextCounter(), promise: promise)
        writes.append(prom)
        if reads.isEmpty && writes.count == 1 {
            state = .writeLocked
            promise.succeed()
        }
        return prom
    }

    func unlockWrite(id: Int) {
        guard let i = writes.firstIndex(where: { $0.id == id }) else {
            assertionFailure()
            return
        }
        assert(i == 0)
        writes.remove(at: i)
        switch (reads.first, writes.first) {
        case let (r?, w?):
            if r.id < w.id {
                state = .readLocked
                for r in reads {
                    guard r.id < w.id else { break }
                    r.promise.succeed()
                }
            } else {
                w.promise.succeed()
            }
        case (_?, nil):
            state = .readLocked
            reads.forEach { $0.promise.succeed() }
        case let (nil, w?):
            w.promise.succeed()
        case (nil, nil):
            state = .done
        }
    }
}

final class FutureLocks {
    private let mut = DispatchSemaphore(value: 1)
    private var mutexes = [String: PromiseLock]()

    private func withLockedGroupsFuture<U: Collection, T>(isRead: Bool, locks: Future<U>, closure: @escaping () -> Future<T>) -> Future<T> where U.Element == String {
        locks.flatMap { self.withLockedGroups(isRead: isRead, locks: $0, eventLoop: locks.eventLoop, closure: closure) }
    }

    func withReadLockedGroupsFuture<U: Collection, T>(locks: Future<U>, closure: @escaping () -> Future<T>) -> Future<T> where U.Element == String {
        withLockedGroupsFuture(isRead: true, locks: locks, closure: closure)
    }

    func withReadLockedGroups<U: Collection, T>(locks: U, eventLoop: EventLoop, closure: @escaping () -> Future<T>) -> Future<T> where U.Element == String {
        self.withLockedGroups(isRead: true, locks: locks, eventLoop: eventLoop, closure: closure)
    }

    func withWriteLockedGroupsFuture<U: Collection, T>(locks: Future<U>, closure: @escaping () -> Future<T>) -> Future<T> where U.Element == String {
        withLockedGroupsFuture(isRead: false, locks: locks, closure: closure)
    }

    func withWriteLockedGroups<U: Collection, T>(locks: U, eventLoop: EventLoop, closure: @escaping () -> Future<T>) -> Future<T> where U.Element == String {
        self.withLockedGroups(isRead: false, locks: locks, eventLoop: eventLoop, closure: closure)
    }

    private func withLockedGroups<U: Collection, T>(isRead: Bool, locks: U, eventLoop: EventLoop, closure: @escaping () -> Future<T>) -> Future<T> where U.Element == String {
        var proms = [(String, Prom)]()
        mut.wait()
        locks.forEach { id in
            let arr = mutexes[id] ?? PromiseLock()
            let lock: (EventLoop) -> Prom = isRead ? arr.lockRead : arr.lockWrite
            proms.append((id, lock(eventLoop)))
            mutexes[id] = arr
        }
        mut.signal()
        let futures = proms.map { $0.1.promise.futureResult }.flatten(on: eventLoop)
        let res = futures.flatMap { closure() }
        let next = res.always {
            self.mut.wait()
            defer { self.mut.signal() }
            proms.forEach { id, prom in
                guard let proms = self.mutexes[id] else {
                    assertionFailure()
                    return
                }
                let unlock: (Int) -> Void = isRead ? proms.unlockRead : proms.unlockWrite
                unlock(prom.id)
                if proms.isFinished {
                    self.mutexes[id] = nil
                }
            }
        }
        return next
    }
}

extension Future where T == Group {
    func withReadLockedGroups<T>(on lock: FutureLocks, do closure: @escaping () -> Future<T>) -> Future<T> {
        map { [$0] }.withReadLockedGroups(on: lock, do: closure)
    }
    func withWriteLockedGroups<T>(on lock: FutureLocks, do closure: @escaping () -> Future<T>) -> Future<T> {
        map { [$0] }.withWriteLockedGroups(on: lock, do: closure)
    }
}

extension Collection where Element == Group {
    func withReadLockedGroups<T>(on lock: FutureLocks, eventLoop: EventLoop, do closure: @escaping () -> Future<T>) -> Future<T> {
        lock.withReadLockedGroups(locks: self.map { $0.id }, eventLoop: eventLoop, closure: closure)
    }
}

extension Future where T: Collection, T.Element == Group {
    func withWriteLockedGroups<T>(on lock: FutureLocks, do closure: @escaping () -> Future<T>) -> Future<T> {
        lock.withWriteLockedGroupsFuture(locks: self.map { $0.map { $0.id } }, closure: closure)
    }
    func withReadLockedGroups<T>(on lock: FutureLocks, do closure: @escaping () -> Future<T>) -> Future<T> {
        lock.withReadLockedGroupsFuture(locks: self.map { $0.map { $0.id } }, closure: closure)
    }
}
