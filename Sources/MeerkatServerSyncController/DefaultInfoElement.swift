//
//  DefaultInfoElement.swift
//  MeerkatServerSyncController
//
//  Created by Filip Klembara on 05/02/2020.
//

public struct DefaultInfoElement: InfoElement {
    public let group: Group

    public let role: Role
}
