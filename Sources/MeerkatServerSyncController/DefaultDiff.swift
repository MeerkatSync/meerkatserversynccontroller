//
//  DefaultDiff.swift
//  MeerkatServerSyncController
//
//  Created by Filip Klembara on 05/02/2020.
//

public struct DefaultDiff: Diff {
    public var groups: [DiffGroupUpdate]

    public var transactions: [Transaction]
}
