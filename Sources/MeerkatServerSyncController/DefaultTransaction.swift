//
//  DefaultTransaction.swift
//  MeerkatServerSyncController
//
//  Created by Filip Klembara on 05/02/2020.
//

public struct DefaultTransaction: Transaction {
    public let deletions: [SubTransaction]

    public let creations: [CreationDescription]

    public let modifications: [SubTransaction]
}
