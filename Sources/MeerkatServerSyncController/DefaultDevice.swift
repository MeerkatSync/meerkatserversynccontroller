//
//  DefaultDevice.swift
//  MeerkatServerSyncControllerTests
//
//  Created by Filip Klembara on 07/02/2020.
//

public struct DefaultDevice: Device {
    public let id: String
    public let description: String
    public init(id: String, description: String = "") {
        self.id = id
        self.description = description
    }
}
