//
//  DefaultGroup.swift
//  MeerkatServerSyncController
//
//  Created by Filip Klembara on 05/02/2020.
//

public struct DefaultGroup: Group {
    public let id: GroupID
    
    public let version: GroupVersion
}
