//
//  MeerkatServerSyncController.swift
//  MeerkatServerSyncController
//
//  Created by Filip Klembara on 05/02/2020.
//

private var lastJobQueue: Future<Void>? = nil
private var jobCount = 0 {
    didSet {
        guard jobCount == 0 else { return }
        lastJobQueue = nil
    }
}


public struct MeerkatServerSyncController {
    public let notificators: Notificators
    public let database: DBWrapper
    private let locks = FutureLocks()

    public init(notificators: Notificators, database: DBWrapper) {
        self.notificators = notificators
        self.database = database
    }

}

extension MeerkatServerSyncController: ServerSyncController {

    public func removeUser(by id: UserID) -> Future<Void> {
        database.user(by: id).flatMap { self.database.remove(user: $0) }
    }

    public func createUser(with id: UserID) -> EventLoopFuture<User> {
        database.addUser(withId: id)
    }

    public func getUser(by id: UserID) -> Future<User> {
        database.user(by: id)
    }

    public func push(_ transaction: Transaction, into groups: [Group], by user: User) -> Future<Diff> {
        let user = getUser(by: user.id)

        let locked = { (allGroups: [Group]) -> Future<Diff> in
            let allGroupsSet = Set(allGroups.map { $0.id })
            let usersGroups = groups.filter { allGroupsSet.contains($0.id) }
            let updateDiff = user.flatMap { self.database.update(groups: usersGroups, with: transaction, by: $0) }
            let usersGroupsSet = Set(groups.map { $0.id })
            let newGroups = allGroups.filter { !usersGroupsSet.contains($0.id) }.map { DefaultGroup(id: $0.id, version: 0) }

            let diffsDiff = user.flatMap { self.database.diffs(for: $0, in: newGroups) }

            let updatedGroupsWithVersion = updateDiff.map { Set($0.groups.map { $0.group }.map { "\($0.id)<>\($0.version)" }) }
            let toNotify = updatedGroupsWithVersion.map { gs in
                usersGroups.filter { !gs.contains("\($0.id)<>\($0.version)") }
            }

            let notifResult = self.notify(groups: toNotify)

            let diff = notifResult.transform(to: updateDiff.and(diffsDiff)).map { u, d -> Diff in
                DefaultDiff(groups: u.groups + d.groups, transactions: u.transactions + d.transactions)
            }
            return diff
        }

        let job = { () -> Future<Diff> in
            let allGroups = groups.withReadLockedGroups(on: self.locks, eventLoop: user.eventLoop) { user.flatMap { self.database.groups(for: $0) } }
            return allGroups.withWriteLockedGroups(on: self.locks) {
                allGroups.flatMap { locked($0) }
            }
        }
        return job()
    }

    public func pull(from groups: [Group], by user: User) -> Future<Diff> {
        let user = getUser(by: user.id)
        let allGroups = groups.withReadLockedGroups(on: locks, eventLoop: user.eventLoop) { user.flatMap { self.database.groups(for: $0) } }

        let job = allGroups.withReadLockedGroups(on: locks) { () -> EventLoopFuture<Diff> in
            let usersGroupsSet = Set(groups.map { $0.id })
            let allGroupsSet = allGroups.map { Set($0.map { $0.id }) }
            let usersGroups = allGroupsSet.map { allGroupsSet in groups.filter { allGroupsSet.contains($0.id) } }
            let newGroups = allGroups.map { gs in gs.filter { !usersGroupsSet.contains($0.id) } }.map { $0.map { DefaultGroup(id: $0.id, version: 0) as Group } }
            let validGroups = usersGroups.and(newGroups).map { $0 + $1 }
            let diff = user.and(validGroups).flatMap { self.database.diffs(for: $0, in: $1) }
            return diff
        }
        return job
    }

    public func getInfo(for user: User) -> Future<Info> {
        let user = getUser(by: user.id)
        let groups = user.flatMap { self.database.groups(for: $0) }
        let a = { groups.flatMap {
            $0.map { g -> Future<InfoElement?> in
                user.flatMap { u -> Future<Role?> in self.database.role(of: u, in: g).map { $0 } }
                    .thenIfErrorThrowing { err in
                        if case DBWrapperError.userNotInGroup = err {
                            return nil
                        }
                        throw err
                }.map { $0.map { DefaultInfoElement(group: g, role: $0) as InfoElement } }
            }.flatten(on: user.eventLoop).map { $0.compactMap { $0 } }
            } }
        return groups.withReadLockedGroups(on: locks, do: a)
    }

    public func createGroup(with id: GroupID, objects: [ObjectDescription], by user: User) -> EventLoopFuture<Group> {
        let user = getUser(by: user.id)

        let addG = user.flatMap { self.database.addGroup(withId: id, owner: $0, objects: objects) }
        return addG
    }

    public func removeGroup(with id: GroupID, by user: User) -> Future<Void> {
        let group = database.group(by: id)
        let getUser = database.user(by: user.id)
        let job = group.withWriteLockedGroups(on: locks) { () -> EventLoopFuture<Void> in
            let users = group.flatMap { self.database.users(for: $0).map { $0.map { $0.user }.filter { $0.id != user.id} } }
            let a = group.and(getUser).flatMap { self.database.remove(group: $0, by: $1) }
            let b = a.transform(to: self.notify(users: users))
            return b
        }
        return job
    }

    public func subscribe(_ newMemberId: UserID, into group: GroupID, by user: User, as role: Role) -> Future<Void> {
        let newMember = getUser(by: newMemberId)
        let user = getUser(by: user.id)
        let group = database.group(by: group)
        let job = group.withWriteLockedGroups(on: locks) { () -> EventLoopFuture<Void> in
            newMember.and(user).and(group).flatMap { self.database.subscribe($0.0, to: $1, by: $0.1, as: role) }
        }
        let b = { newMember.and(user).flatMap { newM, u -> Future<Void> in
            if newM.id != u.id {
                return self.notify(users: newMember.map { [$0] })
            } else {
                return .done(on: newMember.eventLoop)
            }
            } }
        return job.flatMap { b() }
    }

    public func unsubscribe(_ oldMember: UserID, from group: GroupID, by user: User) -> Future<Void> {
        let oldMember = getUser(by: oldMember)
        let group = database.group(by: group)
        let user = getUser(by: user.id)
        let job = group.withWriteLockedGroups(on: locks) { () -> EventLoopFuture<Void> in
            oldMember.and(user).and(group).flatMap { self.database.unsubscribe($0.0, from: $1, by: $0.1) }
        }

        let b = { oldMember.and(user).flatMap { newM, u -> Future<Void> in
            if newM.id != u.id {
                return self.notify(users: oldMember.map { [$0] })
            } else {
                return .done(on: oldMember.eventLoop)
            }
            } }
        return job.flatMap { b() }
    }

    public func users(for group: GroupID, by user: User) -> Future<[UserRole]> {
        let user = getUser(by: user.id)
        let group = database.group(by: group)
        let users = group.withReadLockedGroups(on: locks) { () -> EventLoopFuture<[UserRole]> in
            group.flatMap { self.database.users(for: $0) }
        }
        let us = user.and(users)
        let check = us.map { u, us -> Bool in
            us.map { $0.user.id }.contains(u.id)
        }
        let job = check.flatMap { ok -> Future<[UserRole]> in
            guard ok else {
                return user.and(group).map(to: [UserRole].self) {
                     throw DBWrapperError.userNotInGroup(user: $0, group: $1)
                }
            }
            return users
        }
        return job
    }
}

private var _default: MeerkatServerSyncController?

extension MeerkatServerSyncController: ServiceType {
    public static func makeService(for container: Container) throws -> MeerkatServerSyncController {
        if _default == nil {
            let dbWrapper = try container.make(DBWrapper.self)
            let notificators = try container.make(Notificators.self)
            _default = .init(notificators: notificators, database: dbWrapper)
        }
        return _default!
    }

    public static var serviceSupports: [Any.Type] {
        [ServerSyncController.self]
    }
}
