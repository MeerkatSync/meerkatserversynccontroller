import XCTest

import MeerkatServerSyncControllerTests

var tests = [XCTestCaseEntry]()
tests += MeerkatServerSyncControllerTests.allTests()
XCTMain(tests)
