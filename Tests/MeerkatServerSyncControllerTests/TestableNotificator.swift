//
//  TestableNotificator.swift
//  MeerkatServerSyncControllerPackageDescription
//
//  Created by Filip Klembara on 05/02/2020.
//

import MeerkatServerCore
import Async

class TestableNotificator {
    var notified: [String: UInt] = [:]
}

extension TestableNotificator: Notificator {
    func canNotify(_ device: Device) -> EventLoopFuture<Bool> {
        defaultWorker.future(true)
    }

    func sendNotification(to device: Device) -> EventLoopFuture<Void> {
        defaultWorker.future().map {
            self.notified[device.id] = (self.notified[device.id] ?? 0) + 1
        }
    }
}

class TestableNotificators {
    let notificators: [Notificator]
    let notificatorDBWrapper: NotificatorDBWrapper

    init(notificatorDBWrapper: NotificatorDBWrapper) {
        self.notificatorDBWrapper = notificatorDBWrapper
        notificators = [TestableNotificator()]
    }

    var get: TestableNotificator {
        return notificators.first! as! TestableNotificator
    }
}

extension TestableNotificators: Notificators {
}
