//
//  TestableStructures.swift
//  MeerkatServerSyncControllerTests
//
//  Created by Filip Klembara on 05/02/2020.
//

import Foundation
import MeerkatCore

struct TestableUser: User {
    let id: UserID
}

struct TestableGroup: Group {
    let id: GroupID
    let version: GroupVersion
}

struct TestableTransaction: Transaction {
    let deletions: [SubTransaction]

    let creations: [CreationDescription]

    let modifications: [SubTransaction]
}

struct TestableSubtransaction: SubTransaction {
    let timestamp: Date

    let updates: [Update]

    let className: String

    let objectId: ObjectID
}

struct TestableCreationDescription: CreationDescription {
    let groupID: GroupID

    let object: SubTransaction
}

struct TestableUpdate: Update {
    let attribute: String

    let value: UpdateValue
}

struct TestableDiff: Diff {
    let groups: [DiffGroupUpdate]

    let transactions: [Transaction]
}

struct TestableGroupUpdate: DiffGroupUpdate {
    let newObjects: [ObjectDescription]

    let group: Group

    let role: Role
}
