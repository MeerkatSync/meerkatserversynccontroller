//
//  TestableDBWrapper.swift
//  MeerkatServerSyncControllerTests
//
//  Created by Filip Klembara on 05/02/2020.
//

import Foundation
import MeerkatServerCore

class TestableDBWrapper {
    private var users: [UserID: User]
    private var groups: [GroupID: Group]
    private var usersByGroup: [GroupID: [User]]
    private var groupsByUser: [UserID: [Group]]
    private var roles: [GroupID: [UserID: Role]]
    private var logs: [GroupID: [UInt64: Transaction]]

    init(refDate: Date = Date() - 3800) {
        let uA = TestableUser(id: "A")
        let uB = TestableUser(id: "B")
        let uC = TestableUser(id: "C")
        let gA = TestableGroup(id: "groupA", version: 1)
        let gB = TestableGroup(id: "groupB", version: 0)
        let gC = TestableGroup(id: "groupC", version: 0)
        let gAB = TestableGroup(id: "groupAB", version: 2)
        groups = ["groupA": gA, "groupAB": gAB, "groupB": gB, "groupC": gC]
        users = ["A": uA, "B": uB, "C": uC]
        usersByGroup = ["groupA": [uA], "groupAB": [uA, uB], "groupB": [uB], "groupC": [uC]]
        groupsByUser = ["A": [gA, gAB], "B": [gB, gAB], "C": [gC]]
        roles = ["groupA": ["A": .owner], "groupB": ["B": .owner], "groupAB": ["A": .owner, "B": .read], "groupC": ["C": .owner]]
        logs = [
            "groupA": [
            0: TestableTransaction(deletions: [], creations: [], modifications: []), 1: TestableTransaction(deletions: [], creations: [TestableCreationDescription(groupID: "groupA", object: TestableSubtransaction(timestamp: refDate - 142, updates: [TestableUpdate(attribute: "name", value: .string("Tom"))], className: "Person", objectId: "oA1"))], modifications: [])],
            "groupAB": [
                0: TestableTransaction(deletions: [], creations: [], modifications: []),
                1: TestableTransaction(deletions: [], creations: [TestableCreationDescription(groupID: "groupAB", object: TestableSubtransaction(timestamp: refDate - 1112, updates: [TestableUpdate(attribute: "name", value: .string("Mark"))], className: "Person", objectId: "oAB1"))], modifications: []),
                2: TestableTransaction(deletions: [],
                                       creations: [],
                                       modifications: [
                                        TestableSubtransaction(timestamp: refDate - 521, updates: [TestableUpdate(attribute: "name", value: .string("Marko"))], className: "Person", objectId: "oAB1")
                                        ]
                    )
            ],
            "groupB": [
            0: TestableTransaction(deletions: [], creations: [], modifications: []),
            ],
            "groupC": [
            0: TestableTransaction(deletions: [], creations: [], modifications: []),
            ]
        ]
    }


}

extension TestableDBWrapper: DBWrapper {
    func remove(user: User) -> EventLoopFuture<Void> {
        defaultWorker.future().map {
            self.users[user.id] = nil
        }
    }

    func addUser(withId: UserID) -> EventLoopFuture<User> {
        defaultWorker.future().map {
            guard self.users[withId] == nil else {
                throw DBWrapperError.userAlreadyExists(id: withId)
            }
            let u = TestableUser(id: withId)
            self.users[withId] = u
            return u
        }
    }


    func update(groups: [Group], with: Transaction, by user: User) -> Future<Diff> {
        return diffs(for: user, in: groups)
    }

    private func future() -> Future<Void> {
        defaultWorker.future()
    }
    func user(by id: UserID) -> Future<User> {
        future().map {
            guard let u = self.users[id] else {
                throw DBWrapperError.unknownUser(id: id)
            }
            return u
        }
    }

    func users(for group: Group) -> Future<[UserRole]> {
        future().map {
            guard let g = self.usersByGroup[group.id] else {
                throw DBWrapperError.unknownGroup(id: group.id)
            }
            let r = g.compactMap { u -> UserRole? in
                guard let r = self.roles[group.id]?[u.id] else {
                    return nil
                }
                return UserRole(user: u, role: r)
            }
            return r
        }
    }

    func diffs(for u: User, in groups: [Group]) -> Future<Diff> {
        future().map {
            guard let sgs = self.groupsByUser[u.id]?.map({ $0.id }) else {
                throw DBWrapperError.unknownUser(id: u.id)
            }
            let gs = groups.filter { sgs.contains($0.id) }
            let updatedGS = gs.compactMap { g in self.logs[g.id]?.filter { $0.key > g.version }.isEmpty ?? false ? nil : g }
            let ts = gs.compactMap { g in self.logs[g.id]?.filter { $0.key > g.version }.map { $0.value } }.flatMap { $0 }
            let curretGS = gs.compactMap { self.groups[$0.id] }.map { TestableGroupUpdate(newObjects: [], group: $0, role: self.roles[$0.id]![u.id]!) }
            let no = updatedGS.map { TestableGroupUpdate(newObjects: self.logs[$0.id]!.flatMap { $0.value.creations.map { $0.object } }, group: $0, role: self.roles[$0.id]![u.id]!) }
            let noDone = no.map { $0.group.id }
            let r = TestableDiff(groups: no + curretGS.filter { !noDone.contains($0.group.id) }, transactions: ts)
            return r
        }
    }

    func groups(for user: User) -> Future<[Group]> {
        future().map {
            guard let g = self.groupsByUser[user.id] else {
                throw DBWrapperError.unknownUser(id: user.id)
            }
            return g
        }
    }

    func group(by g: GroupID) -> Future<Group> {
        future().map {
            guard let gg = self.groups[g] else {
                throw DBWrapperError.unknownGroup(id: g)
            }
            return gg
        }
    }

    func subscribe(_ u: User, to: Group, by: User, as: Role) -> Future<Void> {
        future().map {
            let u1 = u
            let u = u.id
            let to = to.id
            guard let r = self.roles[to]?[by.id], r == .owner else {
                throw DBWrapperError.wrongPermissions(for: u1, is: .read, minimum: .owner)
            }
            guard let g1 = self.groupsByUser[by.id]?.filter({ $0.id == to }) else {
                throw DBWrapperError.unknownUser(id: by.id)
            }
            guard let g = g1.first else {
                throw DBWrapperError.unknownGroup(id: to)
            }
            var a = self.usersByGroup[to] ?? []
            a.append(TestableUser(id: u))
            self.usersByGroup[to] = a
            var b = self.groupsByUser[u] ?? []
            b.append(g)
            self.groupsByUser[u] = b
            var c = self.roles[g.id] ?? [:]
            c[u] = `as`
            self.roles[g.id] = c
        }
    }

    func unsubscribe(_ u: User, from: Group, by: User) -> Future<Void> {
        future().map {
            let u1 = u
            let u = u.id
            let from = from.id
            guard let gu = self.groupsByUser[u], gu.contains(where: { $0.id == from }) else {
                return
            }
            guard u == by.id || self.roles[from]?[by.id] == .some(.owner) else {
                throw DBWrapperError.wrongPermissions(for: u1, is: .read, minimum: .owner)
            }
            self.usersByGroup[from] = self.usersByGroup[from]?.filter { $0.id != u } ?? []
            self.groupsByUser[u] = self.groupsByUser[u]?.filter { $0.id != from } ?? []
            self.roles[from] = self.roles[from]?.filter { $0.key != u } ?? [:]
            if let ug = self.usersByGroup[from], ug.isEmpty {
                self.usersByGroup[from] = nil
                self.roles[from] = nil
                self.groups = self.groups.filter { $0.key != from }
            }
        }
    }

    func addGroup(withId: GroupID, owner: User, objects: [ObjectDescription]) -> Future<Group> {
        future().map {
            guard self.users[owner.id] != nil else {
                throw DBWrapperError.unknownUser(id: owner.id)
            }
            guard self.groups[withId] == nil else {
                throw DBWrapperError.groupAlreadyExists(id: withId)
            }
            self.groups[withId] = TestableGroup(id: withId, version: 0)
            self.usersByGroup[withId] = [owner]
            self.groupsByUser[owner.id] = (self.groupsByUser[owner.id] ?? [] ) + [self.groups[withId]!]
            var a = self.roles[withId] ?? [:]
            a[owner.id] = .owner
            self.roles[withId] = a
            return self.groups[withId]!
        }
    }

    func remove(group: Group, by: User) -> Future<Void> {
        future().map {
            let withId = group.id
            guard self.roles[withId]?[by.id] == .owner else {
                throw DBWrapperError.wrongPermissions(for: by, is: .read, minimum: .owner)
            }
            guard let us = self.usersByGroup[withId] else {
                return
            }
            us.forEach { u in
                let a = self.groupsByUser[u.id] ?? []
                let b = a.filter { $0.id != withId }
                self.groupsByUser[u.id] = b
            }
            self.roles[withId] = nil
            self.usersByGroup[withId] = nil
        }
    }

    func role(of u: User, in g: Group) -> Future<Role> {
        future().map {
            guard let gr = self.roles[g.id] else {
                throw DBWrapperError.unknownGroup(id: g.id)
            }
            guard let ur = gr[u.id] else {
                throw DBWrapperError.unknownUser(id: u.id)
            }
            return ur
        }
    }
}
