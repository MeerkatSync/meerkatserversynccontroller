//
//  DefaultWorker.swift
//  MeerkatServerSyncControllerTests
//
//  Created by Filip Klembara on 07/02/2020.
//

import Async

let defaultWorker = MultiThreadedEventLoopGroup(numberOfThreads: 1)
