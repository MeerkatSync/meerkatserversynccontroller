//
//  TestableNotificatorDBWrapper.swift
//  MeerkatServerSyncControllerTests
//
//  Created by Filip Klembara on 05/02/2020.
//

import MeerkatServerCore
import MeerkatServerSyncController
import Async

class TestableNotificatorDBWrapper {
    var users: [String: [String]]

    init(users: [String: [String]] = [:]) {
        self.users = users
    }
}

extension TestableNotificatorDBWrapper: NotificatorDBWrapper {

    func add(device: Device, for user: User) -> EventLoopFuture<Void> {
        defaultWorker.future().map {
            if let devices = self.users[user.id] {
                self.users[user.id] = devices + [device.id]
            } else {
                self.users[user.id] = [device.id]
            }
        }
    }

    func remove(deviceId: String, for user: User) -> EventLoopFuture<Void> {
        defaultWorker.future().map {
            guard let u = self.users[user.id] else {
                throw DBWrapperError.unknownUser(id: user.id)
            }
            self.users[user.id] = u.filter { $0 != deviceId }
        }
    }

    func devices(for user: User) -> EventLoopFuture<[Device]> {
        defaultWorker.future().map { _ -> [Device] in
            guard let a = self.users[user.id] else {
                throw DBWrapperError.unknownUser(id: user.id)
            }
            return a.map { DefaultDevice(id: $0) }
        }
    }

    func canNotify(user: User) -> EventLoopFuture<Bool> {
        defaultWorker.future(users[user.id] != nil)
    }
}
