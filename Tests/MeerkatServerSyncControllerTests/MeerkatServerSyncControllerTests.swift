import XCTest
@testable import MeerkatServerSyncController
import MeerkatServerCore

final class MeerkatServerSyncControllerTests: XCTestCase {

    func createController() -> ServerSyncController {
        let db = TestableDBWrapper()
        let notDB = TestableNotificatorDBWrapper(users: ["A": ["device1", "device2"], "B" : ["device3"], "C": ["device4"], "D": ["device5"]])
        let not = TestableNotificators(notificatorDBWrapper: notDB)
        return MeerkatServerSyncController(notificators: not, database: db)
    }

    func testAddGroup1() {
        let controller = createController()
        let user = TestableUser(id: "C")
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 1)
        XCTAssertNil(controller.createGroup(with: "groupC2", objects: [], by: user).asResult.asFailure)
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 2)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testAddGroup2() {
        let controller = createController()
        let user = TestableUser(id: "A")
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 2)
        XCTAssertNil(controller.createGroup(with: "groupA2", objects: [], by: user).asResult.asFailure)
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 3)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testAddGroup3() {
        let controller = createController()
        let user = TestableUser(id: "D")
        XCTAssertNil(controller.database.groups(for: user).asResult.asSuccess)
        let r = controller.createGroup(with: "groupD1", objects: [], by: user).asTestResult
        XCTAssertEqual(r.asFailure, .unknownUser)
        XCTAssertNil(controller.database.groups(for: user).asResult.asSuccess)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testAddGroup4() {
        let controller = createController()
        let user = TestableUser(id: "A")
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 2)
        let r = controller.createGroup(with: "groupB", objects: [], by: user)
        XCTAssertEqual(r.asTestResult.asFailure, .groupAlreadyExists)
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 2)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testRemoveGroup1() {
        let controller = createController()
        let user = TestableUser(id: "A")
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 2)
        XCTAssertNil(controller.removeGroup(with: "groupA", by: user).asResult.asFailure)
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 1)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testRemoveGroup2() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let userB = TestableUser(id: "B")
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 2)
        XCTAssertEqual(controller.database.groups(for: userB).get?.count, 2)
        XCTAssertNil(controller.removeGroup(with: "groupAB", by: user).asResult.asFailure)
        XCTAssertEqual(controller.database.groups(for: user).get?.count, 1)
        XCTAssertEqual(controller.database.groups(for: userB).get?.count, 1)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, ["device3": 1])
    }

    func testSubscribe1() {
        let controller = createController()
        let user = TestableUser(id: "A")
        XCTAssertEqual(controller.database.users(for: TestableGroup(id: "groupAB", version: 0)).get?.count, 2)
        XCTAssertNoThrow(controller.subscribe("C", into: "groupAB", by: user, as: .owner).asResult.asFailure)
        XCTAssertEqual(controller.database.users(for: TestableGroup(id: "groupAB", version: 0)).get?.count, 3)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, ["device4": 1])
    }

    func testSubscribe2() {
        let controller = createController()
        let user = TestableUser(id: "B")
        XCTAssertEqual(controller.database.users(for: TestableGroup(id: "groupAB", version: 0)).get?.count, 2)
        let r = controller.subscribe("C", into: "groupAB", by: user, as: .owner)
        XCTAssertNil(r.asResult.asSuccess)
        XCTAssertEqual(r.asTestResult.asFailure, .wrongPermissions)
        XCTAssertEqual(controller.database.users(for: TestableGroup(id: "groupAB", version: 0)).get?.count, 2)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testSubscribe3() {
        let controller = createController()
        let user = TestableUser(id: "B")

        let r = controller.subscribe("C", into: "groupD", by: user, as: .owner)
        XCTAssertNil(r.asResult.asSuccess)
        XCTAssertEqual(r.asTestResult.asFailure, .unknownGroup)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testUnsubscribeAFromAByA() {
        let controller = createController()
        let user = TestableUser(id: "A")
        XCTAssertNotNil(controller.database.group(by: "groupA").get)
        let r = controller.unsubscribe(user.id, from: "groupA", by: user).asResult
        XCTAssertNil(r.asFailure)
        XCTAssertNil(controller.database.group(by: "groupA").get)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testUnsubscribeBFromABByA() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let userB = TestableUser(id: "B")
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.groups(for: userB).get?.count, 2)
        let r = controller.unsubscribe(userB.id, from: "groupAB", by: user).asResult
        XCTAssertNil(r.asFailure)
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.groups(for: userB).get?.count, 1)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, ["device3": 1])
    }

    func testUnsubscribeBFromABByB() {
        let controller = createController()
        let userB = TestableUser(id: "B")
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.groups(for: userB).get?.count, 2)
        let r = controller.unsubscribe(userB.id, from: "groupAB", by: userB).asResult
        XCTAssertNil(r.asFailure)
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.groups(for: userB).get?.count, 1)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testUnsubscribeAFromABByB() {
        let controller = createController()
        let userA = TestableUser(id: "A")
        let userB = TestableUser(id: "B")
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.groups(for: userA).get?.count, 2)
        let r = controller.unsubscribe(userA.id, from: "groupAB", by: userB).asTestResult
        XCTAssertEqual(r.asFailure, .wrongPermissions)
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.groups(for: userA).get?.count, 2)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testUnsubscribeAFromDByB() {
        let controller = createController()
        let userA = TestableUser(id: "A")
        let userB = TestableUser(id: "B")
        XCTAssertNil(controller.database.group(by: "groupD").get)
        let r = controller.unsubscribe(userA.id, from: "groupD", by: userB).asTestResult
        XCTAssertEqual(r.asFailure, .unknownGroup)
        XCTAssertNil(controller.database.group(by: "groupD").get)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testUnsubscribeDFromABByB() {
        let controller = createController()
        let userA = TestableUser(id: "A")
        let userD = TestableUser(id: "D")
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)
        XCTAssertEqual(controller.database.users(for: TestableGroup(id: "groupAB", version: 0)).get?.count, 2)
        let r = controller.unsubscribe(userD.id, from: "groupAB", by: userA).asTestResult
        XCTAssertEqual(r.asFailure, .unknownUser)
        XCTAssertEqual(controller.database.users(for: TestableGroup(id: "groupAB", version: 0)).get?.count, 2)
        XCTAssertNotNil(controller.database.group(by: "groupAB").get)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testGetUsers() {
        let controller = createController()

        XCTAssertNotNil(controller.getUser(by: "A"))
        XCTAssertEqual(controller.getUser(by: "A").get?.id, "A")

        XCTAssertNotNil(controller.getUser(by: "B"))
        XCTAssertEqual(controller.getUser(by: "B").get?.id, "B")

        XCTAssertNotNil(controller.getUser(by: "C"))
        XCTAssertEqual(controller.getUser(by: "C").get?.id, "C")

        XCTAssertNil(controller.getUser(by: "D").get)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testGetInfoA() {
        let controller = createController()
        let userA = TestableUser(id: "A")
        let r = controller.getInfo(for: userA).asResult

        XCTAssertNil(r.asFailure)
        XCTAssertTrue(r.isSuccess)
        guard let res = r.asSuccess else {
            XCTFail()
            return
        }
        let g1 = "groupA<>1<>owner"
        let g2 = "groupAB<>2<>owner"
        let gExp = Set([g1, g2])
        let g = Set(res.map { "\($0.group.id)<>\($0.group.version)<>\($0.role)" })
        XCTAssertEqual(g, gExp)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testGetInfoB() {
        let controller = createController()
        let userB = TestableUser(id: "B")
        let r = controller.getInfo(for: userB).asResult

        XCTAssertNil(r.asFailure)
        XCTAssertTrue(r.isSuccess)
        guard let res = r.asSuccess else {
            XCTFail()
            return
        }
        let g1 = "groupB<>0<>owner"
        let g2 = "groupAB<>2<>read"
        let gExp = Set([g1, g2])
        let g = Set(res.map { "\($0.group.id)<>\($0.group.version)<>\($0.role)" })
        XCTAssertEqual(g, gExp)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testGetInfoC() {
        let controller = createController()
        let userC = TestableUser(id: "C")
        let r = controller.getInfo(for: userC).asResult

        XCTAssertNil(r.asFailure)
        XCTAssertTrue(r.isSuccess)
        guard let res = r.asSuccess else {
            XCTFail()
            return
        }
        let g1 = "groupC<>0<>owner"
        let gExp = Set([g1])
        let g = Set(res.map { "\($0.group.id)<>\($0.group.version)<>\($0.role)" })
        XCTAssertEqual(g, gExp)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testGetInfoD() {
        let controller = createController()
        let userD = TestableUser(id: "D")
        let r = controller.getInfo(for: userD).asTestResult

        XCTAssertNil(r.asSuccess)
        XCTAssertTrue(r.isFailure)
        XCTAssertEqual(r.asFailure, .unknownUser)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPull1() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g1 = TestableGroup(id: "groupA", version: 1)
        let g2 = TestableGroup(id: "groupAB", version: 2)
        let res = controller.pull(from: [g1, g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 0)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPull2() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g2 = TestableGroup(id: "groupAB", version: 2)
        let res = controller.pull(from: [g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 1)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPull3() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g2 = TestableGroup(id: "groupAB", version: 1)
        let res = controller.pull(from: [g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 2)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPull4() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let res = controller.pull(from: [], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 3)

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPull5() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g2 = TestableGroup(id: "groupB", version: 0)
        let res = controller.pull(from: [g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 3)
        XCTAssertFalse(diff.groups.map { $0.group.id }.contains("groupB"))

        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPull6() {
        let controller = createController()
        let user = TestableUser(id: "D")
        let g2 = TestableGroup(id: "groupB", version: 0)
        let res = controller.pull(from: [g2], by: user)
        XCTAssertNil(res.asResult.asSuccess)
        XCTAssertTrue(res.asResult.isFailure)
        XCTAssertEqual(res.asTestResult.asFailure, .unknownUser)
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPush1() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g1 = TestableGroup(id: "groupA", version: 1)
        let g2 = TestableGroup(id: "groupAB", version: 2)
        let res = controller.push(DefaultTransaction(deletions: [], creations: [], modifications: []), into: [g1, g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 0)
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPush2() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g2 = TestableGroup(id: "groupAB", version: 2)
        let res = controller.push(DefaultTransaction(deletions: [], creations: [], modifications: []), into: [g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 1)
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPush3() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g2 = TestableGroup(id: "groupAB", version: 1)
        let res = controller.push(DefaultTransaction(deletions: [], creations: [], modifications: []), into: [g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 2)
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPush4() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let res = controller.push(DefaultTransaction(deletions: [], creations: [], modifications: []), into: [], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 3)
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPush5() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let g2 = TestableGroup(id: "groupB", version: 0)
        let res = controller.push(DefaultTransaction(deletions: [], creations: [], modifications: []), into: [g2], by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let diff = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.groups.count, 2)
        XCTAssertEqual(diff.transactions.count, 3)
        XCTAssertFalse(diff.groups.map { $0.group.id }.contains("groupB"))
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testPush6() {
        let controller = createController()
        let user = TestableUser(id: "D")
        let g2 = TestableGroup(id: "groupB", version: 0)
        let res = controller.push(DefaultTransaction(deletions: [], creations: [], modifications: []), into: [g2], by: user)
        XCTAssertNil(res.asResult.asSuccess)
        XCTAssertTrue(res.asResult.isFailure)
        XCTAssertEqual(res.asTestResult.asFailure, .unknownUser)
        let notificator = controller.notificators as! TestableNotificators
        XCTAssertEqual(notificator.get.notified, [:])
    }

    func testUsersForGroup1() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let group = "groupAB"
        let res = controller.users(for: group, by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let users = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(users.count, 2)
        XCTAssertTrue(users.map { $0.user.id }.contains("A"))
        XCTAssertTrue(users.map { $0.user.id }.contains("B"))
    }

    func testUsersForGroup2() {
        let controller = createController()
        let user = TestableUser(id: "B")
        let group = "groupAB"
        let res = controller.users(for: group, by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let users = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(users.count, 2)
        XCTAssertTrue(users.map { $0.user.id }.contains("A"))
        XCTAssertTrue(users.map { $0.user.id }.contains("B"))
    }

    func testUsersForGroup3() {
        let controller = createController()
        let user = TestableUser(id: "B")
        let group = "groupB"
        let res = controller.users(for: group, by: user).asResult
        XCTAssertNil(res.asFailure)
        XCTAssertTrue(res.isSuccess)
        guard let users = res.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(users.count, 1)
        XCTAssertTrue(users.first?.user.id == "B")
    }

    func testUsersForGroup4() {
        let controller = createController()
        let user = TestableUser(id: "A")
        let group = "groupB"
        let res = controller.users(for: group, by: user).asTestResult
        XCTAssertNil(res.asSuccess)
        XCTAssertTrue(res.isFailure)
        guard let err = res.asFailure else {
            XCTFail()
            return
        }
        XCTAssertEqual(err, .userNotInGroup)
    }

    static var allTests = [
    ("testAddGroup1", testAddGroup1),
    ("testAddGroup2", testAddGroup2),
    ("testAddGroup3", testAddGroup3),
    ("testAddGroup4", testAddGroup4),
    ("testRemoveGroup1", testRemoveGroup1),
    ("testRemoveGroup2", testRemoveGroup2),
    ("testSubscribe1", testSubscribe1),
    ("testSubscribe2", testSubscribe2),
    ("testSubscribe3", testSubscribe3),
    ("testUnsubscribeAFromAByA", testUnsubscribeAFromAByA),
    ("testUnsubscribeBFromABByA", testUnsubscribeBFromABByA),
    ("testUnsubscribeBFromABByB", testUnsubscribeBFromABByB),
    ("testUnsubscribeAFromABByB", testUnsubscribeAFromABByB),
    ("testUnsubscribeAFromDByB", testUnsubscribeAFromDByB),
    ("testUnsubscribeDFromABByB", testUnsubscribeDFromABByB),
    ("testGetInfoA", testGetInfoA),
    ("testGetInfoB", testGetInfoB),
    ("testGetInfoC", testGetInfoC),
    ("testGetInfoD", testGetInfoD),
    ("testPull1", testPull1),
    ("testPull2", testPull2),
    ("testPull3", testPull3),
    ("testPull4", testPull4),
    ("testPull5", testPull5),
    ("testPull6", testPull6),
    ("testPush1", testPush1),
    ("testPush2", testPush2),
    ("testPush3", testPush3),
    ("testPush4", testPush4),
    ("testPush5", testPush5),
    ("testPush6", testPush6),
    ("testUsersForGroup1", testUsersForGroup1),
    ("testUsersForGroup2", testUsersForGroup2),
    ("testUsersForGroup3", testUsersForGroup3),
    ("testUsersForGroup4", testUsersForGroup4),
    ]
}

extension Future {
    var get: T? {
        try? wait()
    }

    var asTestResult: Result<T, TestableError> {
        switch asResult {
        case .success(let s):
            return .success(s)
        case .failure(let e):
            return .failure(TestableError(dbWrapperError: e))
        }
    }

    var asResult: Result<T, DBWrapperError> {
        do {
            let s = try wait()
            return .success(s)
        } catch let error as DBWrapperError {
            return .failure(error)
        } catch {
            return .failure(.databaseError(error))
        }
    }
}

enum TestableError: Error {
    case unknownUser
    case unknownGroup
    case wrongPermissions
    case groupAlreadyExists
    case userNotInGroup
    case databaseError
    case userAlreadyExists
    case unknownObject
    init(dbWrapperError: DBWrapperError) {
        switch dbWrapperError {
        case .unknownUser:
            self = .unknownUser
        case .unknownGroup:
            self = .unknownGroup
        case .wrongPermissions:
            self = .wrongPermissions
        case .groupAlreadyExists:
            self = .groupAlreadyExists
        case .userNotInGroup:
            self = .userNotInGroup
        case .databaseError:
            self = .databaseError
        case .userAlreadyExists:
            self = .userAlreadyExists
        case .unknownObject:
            self = .unknownObject
        }
    }
}


extension Result {
    var asFailure: Failure? {
        switch self {
        case .success(_):
            return nil
        case .failure(let e):
            return e
        }
    }

    var asSuccess: Success? {
        switch self {
        case .success(let s):
            return s
        case .failure:
            return nil
        }
    }

    var isSuccess: Bool {
        asSuccess != nil
    }
    var isFailure: Bool {
        !isSuccess
    }
}
