// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatServerSyncController",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatServerSyncController",
            targets: ["MeerkatServerSyncController"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatservercore", from: "1.0.0")
    ],
    targets: [
        .target(
            name: "MeerkatServerSyncController",
            dependencies: ["MeerkatServerCore"]),
        .testTarget(
            name: "MeerkatServerSyncControllerTests",
            dependencies: ["MeerkatServerSyncController"]),
    ]
)
